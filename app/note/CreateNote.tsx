'use client';

import { useRouter } from "next/navigation";
import { useState } from "react";


export default function CreateNote(){
    const [title, setTitle] = useState("");
    const [content, setContent] = useState("");
    const router = useRouter();
    
    const create = async(e)=> {
        e.preventDefault()

        const res = await fetch(
            'http://127.0.0.1:8090/api/collections/notes/records',
            {
                method :'POST',
                headers:{
                    'Content-Type': 'application/json'
                },
                body:JSON.stringify({
                    title,
                    content
                })
            }
        );
        const data = await res.json();
        console.log(data);
        setTitle('');
        setContent('');
        router.refresh();
    }
    
    return <form onSubmit={(e)=>create(e)} >
        <h3>Create new note</h3>
        <input 
            type="text"
            placeholder="title"
            value={title}
            onChange={(e)=>setTitle(e.target.value)}
        />
        <textarea
            placeholder="content"
            value={content}
            onChange={(e)=>setContent(e.target.value)}
        />
        <button type="submit" >
            Create
        </button>
    </form>
}