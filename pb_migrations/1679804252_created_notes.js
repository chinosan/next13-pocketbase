migrate((db) => {
  const collection = new Collection({
    "id": "jm5yrfg727fjzek",
    "created": "2023-03-26 04:17:32.126Z",
    "updated": "2023-03-26 04:17:32.126Z",
    "name": "notes",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "hspcbj3w",
        "name": "title",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": 60,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "opj2zy3m",
        "name": "content",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      }
    ],
    "listRule": "",
    "viewRule": null,
    "createRule": null,
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("jm5yrfg727fjzek");

  return dao.deleteCollection(collection);
})
